const jwt = require('jsonwebtoken');
const secret = 'hdaiu$$^%67777siojvIIUfbvheiruejwrkekdmfvdnahsrfw8urewriwjUTYt$@#@R^T&T'

module.exports = {

    //Helper for login
    signToken: async (input) => {
        const token = await jwt.sign({
            data: input
        }, secret);
        return token;
    },
    
    //Helper for verify token
    verifyToken: async (token) => {
        try {
            var decoded = await jwt.verify(token, secret);
            if (decoded) {
                return decoded;
            } else {
                return null;
            }
        } catch (err) {
            console.log(err)
            return null;
        }
    },

    resetPasswordToken: async (user_id) => {
        const token = await jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            data: user_id
        }, secret);
        return token;
    },

    //Check to make sure header is not undefined, if so, return Forbidden (403)
    checkToken:async(input, next) => {
        const header = input.headers['authorization'];

        if(typeof header !== 'undefined') {

            const bearer = header.split(' ');
            const token = bearer[1];

            input.token = token;

            next();
        } else {
            //If header is undefined return Forbidden (403)
            res.sendStatus(403)
        }
    },

    validateToken: (req, res, next) => {
        console.log('im called')
        const authorizationHeaader = req.headers.authorization;
        let result;
        if (authorizationHeaader) {
          const token = req.headers.authorization.split(' ')[1]; // Bearer <token>
          const options = {
            expiresIn: '2d'
          };
          try {
            // verify makes sure that the token hasn't expired and has been issued by us
            result = jwt.verify(token, secret, options);
    
            // Let's pass back the decoded token to the request object
            req.decoded = result;
            console.log('result ==>',result)
            // We call next to pass execution to the subsequent middleware
            next();
          } catch (err) {
            // Throw an error just in case anything goes wrong with verification
            throw new Error(err);
          }
        } else {
          result = { 
            error: `Authentication error. Token required.`,
            status: 401
          };
          res.status(401).send(result);
        }
    }
}