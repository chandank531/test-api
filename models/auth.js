"use strict"

const db = require('../config/db').db;
const jwtHandler =  require('../utils/jwt-handler')
const passwordHandler = require('../utils/password-handler')

module.exports = {
    login: async (input) => {
        console.log('user login request payload ==>', input)
        try {
            input.mobile_no = input.mobile_no;

            let returnVal = await db.one('SELECT CUSTOMER_ID,CUSTOMER_NAME,MOBILE_NO,CUSTOMER_EMAIL,USER_PASSWORD FROM CUSTOMERS WHERE MOBILE_NO = $1', [input.mobile_no])
            console.log(returnVal)
        

            let isValidPassword = await passwordHandler.comparePassword(input.user_password, returnVal.user_password)
            if (!isValidPassword) return {
                status: false,
                message: 'WrongPassword'
            }

            let token = await jwtHandler.signToken(returnVal)
            return {
                status: true,
                data: {
                    customer_id: returnVal.customer_id,
                    customer_name: returnVal.customer_name,
                    mobile_no: returnVal.mobile_no,
                    customer_email: returnVal.customer_email
                },
                token: token,
                message: 'success'
            }
        } catch (error) {
            console.log('user login request error ==>', error)
            return {
                status: false,
                message: 'User does not exist'
            }
        }
    }
}