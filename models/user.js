"use strict"

const db = require('../config/db').db;
const passwordHandler = require('../utils/password-handler')
const request = require('request');
const util = require('util');

const checkUser = async (input) => {
    let mobileNumber = /^\d{10}$/;
    if ((!input.mobile_no.match(mobileNumber))) {
        return {
            status:false,
            message:'InvalidInput'
        }
    }else {
        let checkUser = await db.any('SELECT COUNT(*) AS USER_CNT,MOBILE_NO,CUSTOMER_EMAIL FROM CUSTOMERS WHERE MOBILE_NO = $1 GROUP BY MOBILE_NO,CUSTOMER_EMAIL', [input.mobile_no])
        console.log('checkuser', checkUser) 
        if (checkUser.length > 0) {
            console.log('im here')
            const dupKeys = []
            checkUser.forEach((item) => {

                if (item.customer_email.trim().toLowerCase() == input.customer_email.trim().toLowerCase()) {
                    dupKeys.push('emailExist')
                }
                if (item.mobile_no.trim() == input.mobile_no.trim()) {
                    dupKeys.push('contactNoExist')
                }
            })
            return {
                status: false,
                data: dupKeys,
                message: 'InvalidUser'
            }
        } else {
            return {
                status: true,
                message: 'ValidUser'
            }
        }
    }
}

const findMissing = async (input) => {
    let sumOfNaturalNumbers = await Math.floor((input.num+1)*(input.num+2)/2)
    for(let i = 0; i < input.num; i++) 
        sumOfNaturalNumbers -= input.userArr[i]

        console.log('sumOfNaturalNumbers',sumOfNaturalNumbers)
        return sumOfNaturalNumbers
        
}
        

module.exports = {

    addUser: async (input,ctx) => {
        input.created_on = new Date()
        // input.created_by = 1111

        try {
            let isValidInput = await checkUser(input);
            console.log('isValidInput', isValidInput)

            if(isValidInput.message == 'InvalidInput') {
                return {
                    status:'false',
                    message: 'Invalid Request'
                }
            }else if(isValidInput.message == 'InvalidUser') {
                if (isValidInput.data.length == 2) {
                    return {
                        status: 'false',
                        message: 'Email & Mobile number already exists',
                    }
                }else if (isValidInput.data.includes('emailExist')) {
                    return {
                        status: 'false',
                        message: 'Email already exists'
                    }
                } else if (isValidInput.data.includes('contactNoExist')) {
                    return {
                        status: 'false',
                        message: 'Mobile number already exists'
                    }
                }
            }else {

                input.user_password = await passwordHandler.encodePassword(input.user_password);

                if (!input.user_password) return {
                    status: false,
                    message: 'failed'
                }

                let returnVal = await db.one("INSERT INTO customers(customer_id,customer_name,mobile_no,customer_email,created_on,created_by,user_password)VALUES(nextval('customer_id'),$1,$2,$3,$4,$5,$6) RETURNING customer_id", [input.customer_name,input.mobile_no, input.customer_email, input.created_on, ctx.customer_id,input.user_password])

                if (!returnVal) return {
                    status: false,
                    message: 'failed'
                }

                return {
                    status: true,
                    data: returnVal,
                    message: 'success'
                }
            }
            
        } catch (error) {
            console.log('register new user request error ==>', error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    getStudentSubject:async(input,ctx) => {
        console.log('======',input)
        try {
            let returnVal = await db.any('select a.customer_id,a.customer_name,b.subject_name from customers_subject_mapping c inner join customers a on c.customer_id = a.customer_id inner join subjects b on c.subject_id = b.subject_id where c.customer_id = $1 order by subject_name asc',[input.customer_id])
            
            console.log('returnVal', returnVal)

            if(!returnVal) return {
                status:false,
                message:'failed'
            }

            return {
                status:true,
                data:returnVal,
                message:'success'
            }
        } catch (error) {
            console.log(error)
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    newObjectCreation:async(input) => {
        
        try {

            const person = {
                id : 2 ,
                gender : 'mail'
            };
                
            const student = {
                name : "ravi" ,
                email :"ravi11@yopmail.com"
            };

            const newObj = await {...person,...student}

            return {
                status:true,
                data:newObj,
                message:'success'
            }
            
        } catch (error) {
            return {
                status: false,
                message: 'InternalServerError'
            }
        }
    },

    missingNumber:async(input,ctx) => {
        console.log('input', input)
        try {
            
            let returnVal = await findMissing(input,ctx);

            return {
                status:true,
                data:returnVal,
                message:'success'
            }
        } catch (error) {
            console.log('error', error)
        }
    },

    promisefiedFunc:async (input) => {
        const url = "http://www.google.com";
        //promisify the request
        let getData = await util.promisify(request);
        let _getData = await getData(url);
        

        console.log('data: ', _getData);

        let content = _getData.body

        return {
            status:true,
            data:content,
            message:'success'
        }

    }
}