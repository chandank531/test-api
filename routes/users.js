const express = require('express');
const router = express.Router();
const userCtrl = require('../controller/user')
const validateToken = require('../utils/jwt-handler').validateToken;


router.post('/add-user', validateToken,async (req, res) => {
  let returnVal = await userCtrl._addUser(req.body,req.decoded.data);

  if (!returnVal.status) res.status(201).json(returnVal)

  res.status(200).json(returnVal)
});

router.post('/student-subject', validateToken,async (req, res) => {
  let returnVal = await userCtrl._getStudentSubject(req.body);

  if (!returnVal.status) res.status(201).json(returnVal)

  res.status(200).json(returnVal)
});

router.post('/new-obj', validateToken,async (req, res) => {
  let returnVal = await userCtrl._newObjectCreation(req.body);

  if (!returnVal.status) res.status(201).json(returnVal)

  res.status(200).json(returnVal)
});

router.post('/missing-num', validateToken,async (req, res) => {
  let returnVal = await userCtrl._missingNumber(req.body);

  if (!returnVal.status) res.status(201).json(returnVal)

  res.status(200).json(returnVal)
});


router.post('/promise-func', validateToken,async (req, res) => {
  let returnVal = await userCtrl._promisefiedFunc(req.body);

  if (!returnVal.status) res.status(201).json(returnVal)

  res.status(200).json(returnVal)
});

module.exports = router;
