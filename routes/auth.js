const express = require('express');
const router = express.Router();
const authCtrl = require('../controller/auth')

/* login */
router.post('/login',async (req, res) => {
  let returnVal = await authCtrl._login(req.body);

  if (!returnVal.status) res.status(500).json(returnVal)

  res.status(200).json(returnVal)
  
});


module.exports = router;
