'use strict';

/*Database credentials*/

const promise = require('bluebird');

const options = {
    promiseLib: promise,
    capSQL: false
};

const pgp = require('pg-promise')(options);


const conString = {
    database: 'test_db',
    port: 5432,
    host: 'localhost',
    user: 'postgres',
    password: 'chandan',
    poolSize: 50,
    poolIdleTimeout: 10000
};


const db = pgp(conString);

module.exports = {
    db: db,
    pgp: pgp
};