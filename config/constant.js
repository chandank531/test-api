'use strict';

//Static path for report generation,file uploads etc ..
const constantPath = {
    reportUrl:"/home/powerguru/Documents/reports/",
    fileUploadPath:"/home/powerguru/Documents/uploads/"
}

//exporting path so that can be reused where ever required
module.exports = {
    reportPath:constantPath.reportUrl,
    fileUploadPath:constantPath.fileUploadPath,
}
