"use strict"

const userModels = require('../models/user');

module.exports = {
    
    _addUser: async (input, ctx) => {
        let returnVal = await userModels.addUser(input,ctx);

        if (!returnVal.status) return {
            status: false,
            data: null,
            message: returnVal.message
        }

        return returnVal;
    },

    _getStudentSubject:async (input,ctx) => {
        let returnVal = await userModels.getStudentSubject(input,ctx);

        if (!returnVal.status) return {
            status: false,
            data: null,
            message: returnVal.message
        }

        return returnVal;
    },

    _newObjectCreation: async(input,ctx) => {
        let returnVal = await userModels.newObjectCreation(input,ctx);

        if (!returnVal.status) return {
            status: false,
            data: null,
            message: returnVal.message
        }

        return returnVal;
    },

    _missingNumber: async(input,n,ctx) => {
        let returnVal = await userModels.missingNumber(input,n,ctx);

        if (!returnVal.status) return {
            status: false,
            data: null,
            message: returnVal.message
        }

        return returnVal;
    },

    _promisefiedFunc: async (input) => {
        let returnVal = await userModels.promisefiedFunc(input);

        if (!returnVal.status) return {
            status: false,
            data: null,
            message: returnVal.message
        }

        return returnVal;
    },
    
}