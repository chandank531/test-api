"use strict";

const authModel = require('../models/auth');

module.exports = {
    
    _login: async (input) => {
        let returnVal = await authModel.login(input);

        if (!returnVal.status) return {
            status: false,
            data: null,
            message: returnVal.message
        }

        return returnVal;
    },
}